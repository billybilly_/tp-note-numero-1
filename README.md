Terence Ardenois & Billy Bombardieri 


Ce TP a pour but de valider nos acquis en conception objet, versionning, UML,
.NET,c# et WinForms.

Dans ce TP nous réaliserons un compteur basique sous la forme d'une application
Winforms.
Le compteur a les fonctionnalités suivantes : 
- Incrémentation du compteur 
- Décrementation du compteur
- Remise a zéro du compteur
